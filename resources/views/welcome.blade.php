<!DOCTYPE html>
<html lang="zxx" class="no-js">
  <head>
    <!-- Mobile Specific Meta -->
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="author" content="CodePixar" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta charset="UTF-8" />
    <title>Test Developer</title>

    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:400,600|Playfair+Display:700,700i"
      rel="stylesheet"
    />
    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="{{asset('assets/css/linearicons.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap-datepicker.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/themify-icons.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}" />

    <!-- ========================================================================== -->
    <link rel="stylesheet" href="{{asset('assets/sneaky/vendors/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/sneaky/vendors/themify-icons/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/sneaky/vendors/owl-carousel/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/sneaky/vendors/owl-carousel/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/sneaky/vendors/Magnific-Popup/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('assets/sneaky/css/style.css')}}">
    <!-- ============================================================================ -->
  </head>

  <body>
    <!--================ Start Header Area =================-->
    <header class="header-area">
      <div class="container">
        <div class="header-wrap">
          <div
            class="header-top d-flex justify-content-between align-items-lg-center navbar-expand-lg"
          >
            <div class="col menu-left">
              <a class="active" href="#">Home</a>
              <a href="#">Category</a>
              <a href="#">Archive</a>
            </div>
            <div class="col-5 text-lg-center mt-2 mt-lg-0">
              <span class="logo-outer">
                <span class="logo-inner">
                  <a href="#"
                    ><img class="mx-auto" src="{{asset('assets/logo.png')}}" alt=""
                  /></a>
                </span>
              </span>
            </div>
            <nav class="col navbar navbar-expand-lg justify-content-end">
              <!-- Toggler/collapsibe Button -->
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#collapsibleNavbar"
              >
                <span class="lnr lnr-menu"></span>
              </button>

              <!-- Navbar links -->
              <div
                class="collapse navbar-collapse menu-right"
                id="collapsibleNavbar"
              >
                <ul class="navbar-nav justify-content-center w-100">
                  <li class="nav-item hide-lg">
                    <a class="nav-link" href="#">Home</a>
                  </li>
                  <li class="nav-item hide-lg">
                    <a class="nav-link" href="#">Category</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="#">Elements</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Blog Detail</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header>
    <!--================ End Header Area =================-->

    <!--================ Start banner Area =================-->
    <div class="container-fluid">
    <div class="col-md-12">
  <section class="hero-banner">
    <div class="hero-wrapper">
      <div class="hero-left">
        <div class="owl-carousel owl-theme hero-carousel">
          <div class="hero-carousel-item">
            <img class="img-fluid" src="{{asset('assets/sneaky/ab.jpg')}}" alt="">
          </div>
          <div class="hero-carousel-item">
            <img class="img-fluid" src="{{asset('assets/sneaky/abc.jpg')}}" alt="">
          </div>
          <div class="hero-carousel-item">
            <img class="img-fluid" src="{{asset('assets/sneaky/abcd.jpg')}}" alt="">
          </div>
          <div class="hero-carousel-item">
            <img class="img-fluid" src="{{asset('assets/sneaky/abcde.jpg')}}" alt="">
          </div>
        </div>
      </div>
      <ul class="social-icons d-none d-lg-block">
        <li><a href="#"><i class="ti-facebook"></i></a></li>
        <li><a href="#"><i class="ti-twitter"></i></a></li>
        <li><a href="#"><i class="ti-instagram"></i></a></li>
      </ul>
      </div>
  </section>
  </div>
  </div>
    <!--================ End banner Area =================-->
    <section class="sample-text-area">
      <div class="container">
        <h3 class="text-heading text-center">Text Sample</h3>
        <p class="sample-text text-center">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur alias minima similique consectetur, perspiciatis voluptatum ipsa neque repellendus quae sed odio cumque aliquid harum officia placeat aliquam esse saepe itaque.
        </p>
      </div>
    </section>

    <!-- =======================slide========================================= -->
    <section class="section-margin mb-lg-100">
      <div class="container">
        <div class="owl-carousel owl-theme featured-carousel">
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/ab.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abc.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abcd.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abcde.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/ab.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abc.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abcd.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
          <div class="featured-item">
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abcde.jpg')}}" alt="">
            <div class="item-body">
                <h5 style="color: black;" class="text-center">Dreadlock Style</h5>
            </div>
          </div>
        </div>
      </div>
    </section>      

    <!-- =======================endslide======================================= -->

    <!-- =============================blog====================================== -->
    <section class="section-margin mb-lg-100">
      <div class="container">
        <div class="owl-carousel owl-theme featured-carousel">
          <div class="featured-item">
            <h5 style="color: black;" class="text-center">Servis</h5>
            <p style="color: black;" class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum quae inventore placeat quibusdam blanditiis, facilis, velit, reprehenderit animi repellendus iste modi laboriosam, quasi at dolor dolore ipsam error veniam aliquam?</p>
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/ab.jpg')}}" alt="">
            <div class="item-body">
                <center><input type="submit" class="btn btn-secondary" value="Click Me"></center>
            </div>
          </div>
          <div class="featured-item">
            <h5 style="color: black;" class="text-center">Servis</h5>
            <p style="color: black;" class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum quae inventore placeat quibusdam blanditiis, facilis, velit, reprehenderit animi repellendus iste modi laboriosam, quasi at dolor dolore ipsam error veniam aliquam?</p>
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abc.jpg')}}" alt="">
            <div class="item-body">
                <center><input type="submit" class="btn btn-secondary" value="Click Me"></center>
            </div>
          </div>
          <div class="featured-item">
            <h5 style="color: black;" class="text-center">Servis</h5>
            <p style="color: black;" class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum quae inventore placeat quibusdam blanditiis, facilis, velit, reprehenderit animi repellendus iste modi laboriosam, quasi at dolor dolore ipsam error veniam aliquam?</p>
            <img class="card-img rounded-0" src="{{asset('assets/sneaky/abcd.jpg')}}" alt="">
            <div class="item-body">
                <center><input type="submit" class="btn btn-secondary" value="Click Me"></center>
            </div>
          </div>
        </div>
      </div>
    </section>      
    <!-- ============================endblog====================================== -->
    <!-- ==========================================footer========================================= -->
    <footer class="footer-area section-gap">
      <div class="container">
        <div class="row">
          <div class="col-xl-3 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
            <ul>
              <li><a href="#">Managed Website</a></li>
              <li><a href="#">Manage Reputation</a></li>
              <li><a href="#">Power Tools</a></li>
              <li><a href="#">Marketing Service</a></li>
            </ul>
          </div>
          <div class="col-xl-3 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
            <ul>
              <li><a href="#">Jobs</a></li>
              <li><a href="#">Brand Assets</a></li>
              <li><a href="#">Investor Relations</a></li>
              <li><a href="#">Terms of Service</a></li>
            </ul>
          </div>
          <div class="col-xl-6 col-md-8 mb-4 mb-xl-0 single-footer-widget">
            <h4>Contact</h4>
            
            <div class="form-wrap" id="mc_embed_signup">
              <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
              method="get">
                <div class="input-group">
                  <input type="email" class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '">
                </div>
                <div class="input-group" style="margin-top: 10px;">
                  <input type="text" class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"></input>
                </div>
                <div class="input-group" style="margin-top: 10px;">
                  <input type="email" class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '">
                </div>
                <div class="input-group" style="margin-top: 10px; ">
                  <button class="btn click-btn btn-right" type="submit">
                      <i class="ti-arrow-right"></i>
                  </button>
                </div>
                <div style="position: absolute; left: -5000px;">
                  <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                </div>

                <div class="info"></div>
              </form>
            </div>
            
          </div>
        </div>
        <div class="footer-bottom row align-items-center text-center text-lg-left">
          <p class="footer-text m-0 col-lg-8 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
        </div>
      </div>
    </footer>
    <!-- =======================================endfooter========================================= -->

    <script src="{{asset('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
      integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
      crossorigin="anonymous"
    ></script>
    <script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.sticky.js')}}"></script>
    <script src="{{asset('assets/js/jquery.tabs.min.js')}}"></script>
    <script src="{{asset('assets/js/parallax.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script
      type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"
    ></script>
    <script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>

    <!-- ======================================================================================== -->
    <script src="{{asset('assets/sneaky/vendors/jquery/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('assets/sneaky/vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/sneaky/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/sneaky/vendors/nice-select/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('assets/sneaky/vendors/Magnific-Popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/sneaky/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{asset('assets/sneaky/js/mail-script.js')}}"></script>
    <script src="{{asset('assets/sneaky/js/main.js')}}"></script>
    <!-- ============================================================================================ -->
  </body>
</html>
